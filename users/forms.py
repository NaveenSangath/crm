
from django import forms
from .models import *




class userF(forms.ModelForm):

    class Meta:
        model=userM
        fields=[

        ]
class studentF(forms.ModelForm):
    first_name = forms.CharField()
    last_name = forms.CharField()
    class Meta:
        model=studentM
        fields=['roll_no','first_name','last_name','accountype','grade','group','personal_email','date_of_birth','age','gender','address','mobile_number','blood_group','dayscholar_or_hostel']

class staffF(forms.ModelForm):
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    class Meta:
        model=staffM
        fields=['uuid','first_name','last_name','accountype','personal_email','date_of_birth','age','gender','address','mobile_number','blood_group','dayscholar_or_hostel']

class schoolF(forms.ModelForm):
    class Meta:
        model=schoolM
        fields=['school_name','school_address','school_board','school_type']
