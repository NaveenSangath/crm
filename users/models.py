# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models
from datetime import *




class userM(AbstractUser):



    STAFF = 0
    PARENT = 1
    PRINCIPLE = 2
    MAINTAINANCE = 3
    STUDENT=4
    ACC = [
        (STAFF, 'STAFF'),
        (PARENT, 'PARENT'),
        (PRINCIPLE, 'PRINCIPLE'),
        (STUDENT,'STUDENT'),
        (MAINTAINANCE, 'MAINTAINANCE')
    ]


    MALE = 1
    FEMALE = 2
    TRANSGENDER = 3
    GENDER = [
         (MALE,'MALE'),
         (FEMALE,'FEMALE'),
         (TRANSGENDER,'TRANSGENDER')
         ]




    uuid = models.CharField(max_length=500, null=True, blank=True)
    accountype = models.CharField(max_length=50, choices=ACC)
    personal_email=models.EmailField(max_length=250,blank=False)
    date_of_birth=models.CharField(max_length=10,blank=False)
    age=models.IntegerField(blank=False)
    gender=models.CharField(max_length=50,blank=False,choices=GENDER)
    address=models.CharField(max_length=500,blank=False)
    pin = models.IntegerField(null=True, blank=True)
    religion = models.CharField(max_length=100)
    mobile_number=models.IntegerField(blank=False)
    blood_group=models.CharField(max_length=6,blank=True)

class schoolM(userM):
    MATRICULATION=0
    STATE_BOARD=1
    CBSE=2
    ICSE=3
    OTHER=4
    SCHOOL_BOARD=(
        (MATRICULATION,'Matriculation'),
        (STATE_BOARD,'State Board'),
        (CBSE,'CBSE'),
        (ICSE,'ICSE'),
        (OTHER,"Other")
    )

    PRIMARY=1
    SECONDARY=2
    HIGHER_SECONDARY=3
    SCHOOL_TYPE=(
        (PRIMARY,'primary'),
        (SECONDARY,'secondary'),
        (HIGHER_SECONDARY,'higher_secondary'))
    school_name = models.CharField(max_length=1000)
    school_board=models.IntegerField(choices=SCHOOL_BOARD,default=OTHER)
    school_type=models.CharField(max_length=50,choices=SCHOOL_TYPE)
    school_address=models.CharField(max_length=1000,blank=False)






class staffM(userM):
    HOSTERLLER = 0
    DAYSCHOLAR = 1
    HOSTELRDAYSCHOLAR = [
        (HOSTERLLER,'HOSTELLER'),
        (DAYSCHOLAR,'DAYSCHOLAR')
             ]
    teacher_id=models.CharField(max_length=100,null=True,blank=True)
    specialization=models.TextField()
    resume_url=models.CharField(max_length=5000,null=True,blank=True)
    dayscholar_or_hostel = models.CharField(max_length=100, blank=False, choices=HOSTELRDAYSCHOLAR)
    #
    # I=1
    # II=2
    # III=3
    # IV=4
    # V=5
    # VI=6
    # VII=7
    # VIII=8
    # IX=9
    # X=10
    # XI=11
    # XII=12
    # CLASS=[
    #        (I,'I'),
    #        (II,'I'),
    #        (III,'III'),
    #        (IV,'IV'),
    #        ( V,'V'),
    #        (VI,'VI'),
    #        (VII,'VII'),
    #        ( VIII,'VIII'),
    #        (IX,'IX'),
    #        (X,'X'),
    #        (XI,'XI'),
    #        (XII,'XII')      ]
    #
    #
    # A=1
    # B=2
    # C=3
    # D=4
    # E=5
    # SECTION=[
    #     (A,'A'),
    #      (B,'B'),
    #      (C,'C'),
    #      (D,'D'),
    #      (E,'E')]

    # incharge_class=models.CharField(max_length=15,choices=CLASS)
    # section=models.CharField(max_length=15,choices=SECTION)   use in staff allotment page



class studentM(userM):
    BIOMATH = 4
    COMPUTERSCIENCE = 5
    ELSE = 6
    GROUP = [
        ( BIOMATH,' BIOMATH'),
        (COMPUTERSCIENCE,'COMPUTERSCIENCE'),
        (ELSE,'ELSE')
            ]


    HOSTERLLER = 0
    DAYSCHOLAR = 1
    HOSTELRDAYSCHOLAR = [
        (HOSTERLLER,'HOSTELLER'),
        (DAYSCHOLAR,'DAYSCHOLAR')
             ]

    I=1
    II=2
    III=3
    IV=4
    V=5
    VI=6
    VII=7
    VIII=8
    IX=9
    X=10
    XI=11
    XII=12
    CLASS=[
           (I,'I'),
           (II,'I'),
           (III,'III'),
           (IV,'IV'),
           ( V,'V'),
           (VI,'VI'),
           (VII,'VII'),
           ( VIII,'VIII'),
           (IX,'IX'),
           (X,'X'),
           (XI,'XI'),
           (XII,'XII')      ]
    parents = models.CharField(max_length=50, default=0)
    admission_no = models.CharField(max_length=1000, null=True, blank=True)
    group = models.CharField(max_length=50, blank=True, choices=GROUP)
    roll_no = models.CharField(max_length=15, blank=False)
    grade = models.CharField(max_length=15, choices=CLASS)
    dayscholar_or_hostel = models.CharField(max_length=100, blank=False, choices=HOSTELRDAYSCHOLAR)
    #
    # A=1
    # B=2
    # C=3
    # D=4
    # E=5
    # SECTION=[
    #     (A,'A'),
    #      (B,'B'),
    #      (C,'C'),
    #      (D,'D'),
    #      (E,'E')]
    # section = models.CharField(max_length=15, choices=SECTION) use in class allotment page